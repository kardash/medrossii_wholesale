<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Блог");
?>
<div class="grid grid--xl _ph-20">
    <div class="cell cell--24">
        <div class="grid _s-15 _mb-0">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:main.include", "", Array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "sidebar",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_MODE" => "html",
                    ), false, Array('HIDE_ICONS' => 'Y')
            );
            ?>
            <div class="cell cell--19 cell--lg-17 cell--ms-24">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:news", "news_list", array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => "1",
                    "TEMPLATE_THEME" => "site",
                    "NEWS_COUNT" => "2",
                    "USE_SEARCH" => "N",
                    "USE_RSS" => "N",
                    "NUM_NEWS" => "20",
                    "NUM_DAYS" => "180",
                    "YANDEX" => "N",
                    "USE_RATING" => "N",
                    "USE_CATEGORIES" => "N",
                    "USE_REVIEW" => "Y",
                    "USE_FILTER" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "CHECK_DATES" => "Y",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => "/blog/",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_PANEL" => "Y",
                    "SET_TITLE" => "Y",
                    "SET_STATUS_404" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "ADD_ELEMENT_CHAIN" => "Y",
                    "USE_PERMISSIONS" => "N",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "LIST_FIELD_CODE" => array(
                        0 => "TAGS",
                        1 => "",
                    ),
                    "LIST_PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "DISPLAY_NAME" => "Y",
                    "META_KEYWORDS" => "-",
                    "META_DESCRIPTION" => "-",
                    "BROWSER_TITLE" => "-",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_PROPERTY_CODE" => array(
                        0 => "NEWS_TAGS",
                        1 => "",
                    ),
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PAGER_TEMPLATE" => "arrows",
                    "DETAIL_PAGER_SHOW_ALL" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Блог",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                    "PAGER_SHOW_ALL" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "SLIDER_PROPERTY" => "",
                    "COMPONENT_TEMPLATE" => "news_list",
                    "SET_LAST_MODIFIED" => "N",
                    "STRICT_SECTION_CHECK" => "N",
                    "USE_SHARE" => "Y",
                    "MEDIA_PROPERTY" => "",
                    "DETAIL_SET_CANONICAL_URL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SHOW_404" => "Y",
                    "MESSAGE_404" => "",
                    "MESSAGES_PER_PAGE" => "10",
                    "USE_CAPTCHA" => "Y",
                    "REVIEW_AJAX_POST" => "Y",
                    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                    "FORUM_ID" => "1",
                    "URL_TEMPLATES_READ" => "",
                    "SHOW_LINK_TO_FORUM" => "N",
                    "FILTER_NAME" => "arFilter",
                    "FILTER_FIELD_CODE" => array(
                        0 => "TAGS",
                        1 => "",
                    ),
                    "FILTER_PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                        2 => "",
                    ),
                    "SHARE_HIDE" => "N",
                    "SHARE_TEMPLATE" => "",
                    "SHARE_HANDLERS" => array(
                        0 => "delicious",
                        1 => "facebook",
                        2 => "lj",
                        3 => "mailru",
                        4 => "twitter",
                        5 => "vk",
                    ),
                    "SHARE_SHORTEN_URL_LOGIN" => "",
                    "SHARE_SHORTEN_URL_KEY" => "",
                    "FILE_404" => "",
                    "SEF_URL_TEMPLATES" => array(
                        "news" => "",
                        "section" => "category/#SECTION_CODE#/",
                        "detail" => "#ELEMENT_CODE#/",
                    )
                        ), false);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
/**
 * SEO
 */
$title = $APPLICATION->GetTitle('title');
$str = '';
if (isset($_GET["PAGEN_1"]) AND $_GET["PAGEN_1"] > 1) {

    $str .= $_GET["PAGEN_1"] . ' страница';
    $APPLICATION->SetPageProperty('title', $title . ' - ' . $str);
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>