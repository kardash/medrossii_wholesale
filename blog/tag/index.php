<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Теги");
?>
<div class="grid grid--xl _ph-20">
    <div class="cell cell--24">
        <div class="grid _s-15 _mb-0">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:main.include", "", Array(
                "AREA_FILE_RECURSIVE" => "Y",
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "sidebar",
                "EDIT_MODE" => "html"
                    ), false, Array(
                'HIDE_ICONS' => 'Y'
                    )
            );
            ?>
            <div class="cell cell--19 cell--lg-17 cell--ms-24">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:news", "tags", array(
                    "ADD_ELEMENT_CHAIN" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_STYLE" => "N",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_PAGER_SHOW_ALL" => "N",
                    "DETAIL_PAGER_TEMPLATE" => "arrows",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_SET_CANONICAL_URL" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PANEL" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FILTER_FIELD_CODE" => array(
                        0 => "TAGS",
                        1 => "",
                    ),
                    "FILTER_NAME" => "arFilter",
                    "FILTER_PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                        2 => "",
                    ),
                    "FORUM_ID" => "2",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "6",
                    "IBLOCK_TYPE" => "references",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "LIST_FIELD_CODE" => array(
                        0 => "TAGS",
                        1 => "",
                    ),
                    "LIST_PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "MEDIA_PROPERTY" => "",
                    "MESSAGES_PER_PAGE" => "10",
                    "MESSAGE_404" => "",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "NEWS_COUNT" => "10",
                    "NUM_DAYS" => "180",
                    "NUM_NEWS" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "arrows",
                    "PAGER_TITLE" => "Блог",
                    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "REVIEW_AJAX_POST" => "Y",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "SEF_FOLDER" => "/blog/tag/",
                    "SEF_MODE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_STATUS_404" => "Y",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N",
                    "SHOW_LINK_TO_FORUM" => "N",
                    "SLIDER_PROPERTY" => "",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "TEMPLATE_THEME" => "site",
                    "URL_TEMPLATES_READ" => "",
                    "USE_CAPTCHA" => "Y",
                    "USE_CATEGORIES" => "N",
                    "USE_FILTER" => "N",
                    "USE_PERMISSIONS" => "N",
                    "USE_RATING" => "N",
                    "USE_REVIEW" => "N",
                    "USE_RSS" => "N",
                    "USE_SEARCH" => "N",
                    "USE_SHARE" => "N",
                    "YANDEX" => "N",
                    "COMPONENT_TEMPLATE" => ".default",
                    "SEF_URL_TEMPLATES" => array(
                        "news" => "",
                        "section" => "category/#SECTION_CODE#/",
                        "detail" => "#ELEMENT_CODE#/",
                    )
                        ), false
                );
                ?>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * SEO
 */
$title = $APPLICATION->GetTitle('title');
$str = '';
if (isset($_GET["PAGEN_1"]) AND $_GET["PAGEN_1"] > 1) {

    $str .= $_GET["PAGEN_1"] . ' страница';
    $APPLICATION->SetPageProperty('title', $title . ' - ' . $str);
}
?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>