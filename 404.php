<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
global $APPLICATION;
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
//$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>

<div class="w-error-page">
    <div class="grid grid--xl _ph-20">
        <div class="cell cell--24">
            <div class="grid _items-center _justify-center _s-20 _ms-s-10">
                <div class="cell cell--12 cell--ms-16 cell--xs-24">
                    <div class="w-error-page__image">
                        <img src="<?php echo LINK_TYPE . SITE_TEMPLATE_PATH; ?>/images/error-404.png" alt="">
                    </div>
                </div>
                <div class="cell cell--12 cell--ms-16 cell--xs-24">
                    <h1 class="w-title w-title--little _mb-20 _ms-text-center">Страница не найдена!</h1>
                    <div class="w-error-page__content w-text _mb-20">
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/text_404.php"), false);
                        ?>
                    </div>
                    <div class="_ms-text-center">
                        <a href="<?php echo LINK_TYPE; ?>" class="w-button">
                            <span>Вернуться на главную</span>
                            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo LINK_TYPE . SITE_TEMPLATE_PATH; ?>/svg/sprite.svg#icon-arrow-right"></use></svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "product_catalog", Array(
    "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
    "CACHE_GROUPS" => "Y", // Учитывать права доступа
    "CACHE_TIME" => "36000000", // Время кеширования (сек.)
    "CACHE_TYPE" => "A", // Тип кеширования
    "COUNT_ELEMENTS" => "Y", // Показывать количество элементов в разделе
    "IBLOCK_ID" => IBLOCK_CATALOG_ID, // Инфоблок
    "IBLOCK_TYPE" => "catalog", // Тип инфоблока
    "SECTION_CODE" => "", // Код раздела
    "SECTION_FIELDS" => array(// Поля разделов
        0 => "",
        1 => "",
    ),
    "SECTION_ID" => $_REQUEST["SECTION_ID"], // ID раздела
    "SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
    "SECTION_USER_FIELDS" => array(// Свойства разделов
        0 => "UF_ICON",
        1 => "",
    ),
    "SHOW_PARENT_NAME" => "Y", // Показывать название раздела
    "TOP_DEPTH" => "1", // Максимальная отображаемая глубина разделов
    "VIEW_MODE" => "LINE", // Вид списка подразделов
        ), false
);
?>

<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>