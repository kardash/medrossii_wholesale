<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Вы искали: «" . $_GET["q"] . '»');
?>

<div class="w-search-result">
    <div class="_hide js-select-word" data-select="<?php echo $_GET["q"]; ?>"></div>
    <div class="grid grid--xl _ph-20">
        <div class="cell cell--24">
            <?php
            $APPLICATION->IncludeComponent(
                    "bitrix:catalog.search", "catalog_search", array(
                "ACTION_VARIABLE" => "action",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BASKET_URL" => "/personal/cart/",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "N",
                "CONVERT_CURRENCY" => "N",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_COMPARE" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
                "ELEMENT_SORT_FIELD2" => "shows",
                "ELEMENT_SORT_ORDER" => "desc",
                "ELEMENT_SORT_ORDER2" => "desc",
                "HIDE_NOT_AVAILABLE" => "N",
                "IBLOCK_ID" => "20",
                "IBLOCK_TYPE" => "catalog",
                "LINE_ELEMENT_COUNT" => "5",
                "NO_WORD_LOGIC" => "N",
                "OFFERS_CART_PROPERTIES" => array(
                    0 => "VES",
                ),
                "OFFERS_FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "OFFERS_LIMIT" => "5",
                "OFFERS_PROPERTY_CODE" => array(
                    0 => "VES",
                    1 => "",
                ),
                "OFFERS_SORT_FIELD" => "sort",
                "OFFERS_SORT_FIELD2" => "id",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_ORDER2" => "desc",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Товары",
                "PAGE_ELEMENT_COUNT" => "6",
                "PRICE_CODE" => array(
                    0 => "WHOLESALE",
                ),
                "PRICE_VAT_INCLUDE" => "N",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPERTIES" => "",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PROPERTY_CODE" => array(
                    0 => "PROMOTION",
                    1 => "CML2_ARTICLE",
                    2 => "YEAR_COLLECTION",
                    3 => "NEWPRODUCT",
                    4 => "COLLECTION_REGION",
                    5 => "HONEY_TYPE",
                    6 => "SALELEADER",
                    7 => "",
                ),
                "RESTART" => "Y",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SECTION_URL" => "",
                "SHOW_PRICE_COUNT" => "1",
                "USE_LANGUAGE_GUESS" => "Y",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "Y",
                "COMPONENT_TEMPLATE" => "catalog_search",
                "HIDE_NOT_AVAILABLE_OFFERS" => "N"
                    ), false
            );
            ?>
        </div>
    </div>
</div>

<br>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>