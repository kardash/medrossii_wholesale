<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<? /* $APPLICATION->IncludeComponent("bitrix:map.google.view", "", Array(
"API_KEY" => "AIzaSyCcm6mMIvpN62W_yao5ncLK7AZ9KqP3iX0",	// Ключ JavaScript API https://developers.google.com/maps/documentation/javascript/get-api-key
"INIT_MAP_TYPE" => "SCHEME",	// Стартовый тип карты
"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.76;s:10:\"google_lon\";d:37.639999999999986;s:12:\"google_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:49:\"sdfkljsdlfkjljk###RN###sdfsdfsdf###RN###sdfsdfsdf\";s:3:\"LON\";d:37.615814208984375;s:3:\"LAT\";d:55.75571349188569;}}}",	// Данные, выводимые на карте
"MAP_WIDTH" => "600",	// Ширина карты
"MAP_HEIGHT" => "500",	// Высота карты
"CONTROLS" => array(	// Элементы управления
0 => "SMALL_ZOOM_CONTROL",
1 => "TYPECONTROL",
2 => "SCALELINE",
),
"OPTIONS" => array(	// Настройки
0 => "ENABLE_SCROLL_ZOOM",
1 => "ENABLE_DBLCLICK_ZOOM",
2 => "ENABLE_DRAGGING",
3 => "ENABLE_KEYBOARD",
),
"MAP_ID" => "gm_1",	// Идентификатор карты
"COMPONENT_TEMPLATE" => ".default"
),
false
); */ ?>



<div class="w-contacts" itemscope="" itemtype="http://schema.org/Organization">
    <?php
    $rsSites = CSite::GetByID(SITE_ID);
    $arSite = $rsSites->Fetch();
    ?>
    <meta itemprop="name" content='<?php echo $arSite["SITE_NAME"]; ?>'>
    <div class="grid grid--xl _ph-20">
        <div class="cell cell--24">
            <div class="grid _sh-10">
                <div class="cell cell--10 cell--md-12 cell--ms-24">
                    <div class="js-contacts-content" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                        <div class="w-contacts__text">
                            <p><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts/text.php"), false); ?></p>
                        </div>
                        <div class="grid _flex-nowrap _s-10 _mt-30 _mb-10">
                            <div class="cell cell--md-0">
                                <div class="w-icon-link w-icon-link--footer">
                                    <i>
                                        <svg><use xlink:href="<?php echo LINK_TYPE . SITE_TEMPLATE_PATH; ?>/svg/sprite.svg#icon-phone"></use></svg>
                                    </i>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid _flex-nowrap _s-15">
                                    <div class="cell cell--24">
                                        <div class="w-contacts__head">Отдел продаж:</div>
                                        <div class="w-contacts__links">
                                            <a href="tel:+78003330502" itemprop="telephone">+7 800 333-05-02</a>
                                            <meta itemprop="telephone" content="+7 800 333-05-02">
                                            <a href="tel:+74993467555" itemprop="telephone">+7 499 346-75-55</a>
                                            <meta itemprop="telephone" content="+7 499 346-75-55">
                                            <a href="tel:+79265374145" itemprop="telephone">+7 926 537-41-45</a>
                                            <meta itemprop="telephone" content="+7 926 537-41-45">
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="w-contacts__head">Отдел закупок:</div>
                                        <div class="w-contacts__links">
                                            <a href="tel:+74957725974" itemprop="telephone">+7 495 772-59-74 </a>
                                            <meta itemprop="telephone" content="+7 495 772-59-74">
                                            <a href="tel:+79262454009" itemprop="telephone">+7 926 245-40-09</a>
                                            <meta itemprop="telephone" content="+7 926 245-40-09">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w-title w-title--small">Наш адрес</div>
                        <div class="w-contacts__text _mt-5">
                            <p><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts/address.php"), false); ?></p>
                        </div>
                        <div class="w-contacts__line _mt-15"></div>
                        <div class="grid _flex-nowrap _s-10 _mt-30 _mb-10">
                            <div class="cell cell--md-0 _flex-noshrink _flex-nogrow">
                                <div class="w-icon-link w-icon-link--footer">
                                    <i>
                                        <svg><use xlink:href="<?php echo LINK_TYPE . SITE_TEMPLATE_PATH; ?>/svg/sprite.svg#icon-location"></use></svg>
                                    </i>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid  _s-15">
                                    <div class="cell">
                                        <div class="w-contacts__head">Адрес склада:</div>
                                        <div class="w-contacts__desc"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts/sklad.php"), false); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--md-0 _flex-noshrink _flex-nogrow">
                                <div class="w-icon-link w-icon-link--footer">
                                    <i>
                                        <svg><use xlink:href="<?php echo LINK_TYPE . SITE_TEMPLATE_PATH; ?>/svg/sprite.svg#icon-phone"></use></svg>
                                    </i>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid  _s-15">
                                    <div class="cell">
                                        <div class="w-contacts__head">Режим работы:</div>
                                        <div class="w-contacts__desc w-contacts__desc--nowrap"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts/work_time1.php"), false); ?></div>
                                        <div class="w-contacts__desc"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts/work_time2.php"), false); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--14 cell--md-12 cell--ms-24">
                    <?php /* <?$APPLICATION->IncludeComponent("bitrix:map.google.view", "", Array(
                      "API_KEY" => "AIzaSyCcm6mMIvpN62W_yao5ncLK7AZ9KqP3iX0",	// Ключ JavaScript API https://developers.google.com/maps/documentation/javascript/get-api-key
                      "INIT_MAP_TYPE" => "SCHEME",	// Стартовый тип карты
                      "MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.76;s:10:\"google_lon\";d:37.639999999999986;s:12:\"google_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:49:\"sdfkljsdlfkjljk###RN###sdfsdfsdf###RN###sdfsdfsdf\";s:3:\"LON\";d:37.615814208984375;s:3:\"LAT\";d:55.75571349188569;}}}",	// Данные, выводимые на карте
                      "MAP_WIDTH" => "600",	// Ширина карты
                      "MAP_HEIGHT" => "500",	// Высота карты
                      "CONTROLS" => array(	// Элементы управления
                      0 => "SMALL_ZOOM_CONTROL",
                      1 => "TYPECONTROL",
                      2 => "SCALELINE",
                      ),
                      "OPTIONS" => array(	// Настройки
                      0 => "ENABLE_SCROLL_ZOOM",
                      1 => "ENABLE_DBLCLICK_ZOOM",
                      2 => "ENABLE_DRAGGING",
                      3 => "ENABLE_KEYBOARD",
                      ),
                      "MAP_ID" => "gm_1",	// Идентификатор карты
                      "COMPONENT_TEMPLATE" => ".default"
                      ),
                      false
                      );?> */ ?>
                    <div class="hide js-map-script" data-key="AIzaSyCcm6mMIvpN62W_yao5ncLK7AZ9KqP3iX0" data-language="ru"></div>
                    <div class="w-map js-map" data-lat="55.8511516" data-lng="37.6795512"></div>
                    <style>
                        @media (min-width: 769px) {
                            .w-contacts .w-map {
                                -webkit-mask-image: url(<?php echo SITE_TEMPLATE_PATH; ?>/svg/contacts-map-mask.svg);
                                mask-image: url(<?php echo SITE_TEMPLATE_PATH; ?>/svg/contacts-map-mask.svg);
                            }
                        }
                    </style>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$APPLICATION->IncludeComponent("wezom:form.result.new", "contacts", Array(
    "SEF_MODE" => "N", // Включить поддержку ЧПУ
    "WEB_FORM_ID" => "5", // ID веб-формы
    "LIST_URL" => "", // Страница со списком результатов
    "EDIT_URL" => "", // Страница редактирования результата
    "SUCCESS_URL" => "", // Страница с сообщением об успешной отправке
    "CHAIN_ITEM_TEXT" => "", // Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "", // Ссылка на дополнительном пункте в навигационной цепочке
    "IGNORE_CUSTOM_TEMPLATE" => "Y", // Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y", // Использовать расширенный вывод сообщений об ошибках
    "CACHE_TYPE" => "A", // Тип кеширования
    "CACHE_TIME" => "3600", // Время кеширования (сек.)
    "VARIABLE_ALIASES" => array(
        "WEB_FORM_ID" => "WEB_FORM_ID",
        "RESULT_ID" => "RESULT_ID",
    ),
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_SHADOW" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "N",
    "AJAX_OPTION_HISTORY" => "N",
    "USER_CONSENT" => "Y",
    "USER_CONSENT_ID" => "2",
    "USER_CONSENT_IS_CHECKED" => "Y",
    "USER_CONSENT_IS_LOADED" => "Y",
        ), false
);
?>

<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>