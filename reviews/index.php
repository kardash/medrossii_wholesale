<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>
<?php $iblockDescription = CIBlock::GetArrayByID(31, "DESCRIPTION"); ?>
<div class="grid grid--xl _ph-20">
    <div class="cell cell--24">
        <div class="grid _sv-15 _mb-20">
            <?php if ($iblockDescription): ?>
                <div class="cell cell--24">
                    <div class="w-faq-text"><?php echo $iblockDescription; ?></div>
                </div>
            <?php endif; ?>
            <div class="cell cell--14 cell--md-12 cell--ms-24">
                <?php
                $APPLICATION->IncludeComponent(
                        "bitrix:news.list", "reviews", array(
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "COMPONENT_TEMPLATE" => "reviews",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "31",
                    "IBLOCK_TYPE" => "-",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "3",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "MARK",
                        1 => "ANSWER_DATE",
                        2 => "",
                    ),
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N"
                        ), false
                );
                ?>
            </div>
            <div class="cell cell--10 cell--md-12 cell--ms-24 _ph-50 _md-ph-20 _ms-ph-0 js-sticky-container">
                <?php
                $APPLICATION->IncludeComponent("wezom:iblock.element.add.form", "reviews", Array(
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "", // * дата начала *
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "", // * дата завершения *
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "", // * подробная картинка *
                    "CUSTOM_TITLE_DETAIL_TEXT" => "", // * подробный текст *
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "", // * раздел инфоблока *
                    "CUSTOM_TITLE_NAME" => "Имя", // * наименование *
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "", // * картинка анонса *
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "Сообщение", // * текст анонса *
                    "CUSTOM_TITLE_TAGS" => "", // * теги *
                    "DEFAULT_INPUT_SIZE" => "30", // Размер полей ввода
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N", // Использовать визуальный редактор для редактирования подробного текста
                    "ELEMENT_ASSOC" => "CREATED_BY", // Привязка к пользователю
                    "GROUPS" => array(// Группы пользователей, имеющие право на добавление/редактирование
                        0 => "2",
                    ),
                    "IBLOCK_ID" => "31", // Инфоблок
                    "IBLOCK_TYPE" => "reviews", // Тип инфоблока
                    "LEVEL_LAST" => "Y", // Разрешить добавление только на последний уровень рубрикатора
                    "LIST_URL" => "", // Страница со списком своих элементов
                    "MAX_FILE_SIZE" => "0", // Максимальный размер загружаемых файлов, байт (0 - не ограничивать)
                    "MAX_LEVELS" => "100000", // Ограничить кол-во рубрик, в которые можно добавлять элемент
                    "MAX_USER_ENTRIES" => "100000", // Ограничить кол-во элементов для одного пользователя
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N", // Использовать визуальный редактор для редактирования текста анонса
                    "PROPERTY_CODES" => array(// Свойства, выводимые на редактирование
                        0 => "278",
                        1 => "279",
                        2 => "NAME",
                        3 => "PREVIEW_TEXT",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(// Свойства, обязательные для заполнения
                        0 => "278",
                        1 => "279",
                        2 => "NAME",
                        3 => "PREVIEW_TEXT",
                    ),
                    "RESIZE_IMAGES" => "N", // Использовать настройки инфоблока для обработки изображений
                    "SEF_MODE" => "N", // Включить поддержку ЧПУ
                    "STATUS" => "ANY", // Редактирование возможно
                    "STATUS_NEW" => "NEW", // Деактивировать элемент
                    "USER_MESSAGE_ADD" => "", // Сообщение об успешном добавлении
                    "USER_MESSAGE_EDIT" => "", // Сообщение об успешном сохранении
                    "USE_CAPTCHA" => "N", // Использовать CAPTCHA
                    "AJAX_MODE" => "Y",
                    "AJAX_OPTION_SHADOW" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "USER_CONSENT" => "Y",
                    "USER_CONSENT_ID" => "2",
                    "USER_CONSENT_IS_CHECKED" => "Y",
                    "USER_CONSENT_IS_LOADED" => "Y",
                        ), false
                );
                ?>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * SEO
 */
$title = $APPLICATION->GetTitle('title');
$str = '';
if (isset($_GET["PAGEN_1"]) AND $_GET["PAGEN_1"] > 1) {

    $str .= $_GET["PAGEN_1"] . ' страница';
    $APPLICATION->SetPageProperty('title', $title . ' - ' . $str);
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>