<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("ROBOTS", "index, follow robots");
$APPLICATION->SetPageProperty("KEYWORDS", "рпрпорпо");
$APPLICATION->SetPageProperty("DESCRIPTION", "пропрорпо");
$APPLICATION->SetTitle("faq123");
?>
<?php $iblockDescription = CIBlock::GetArrayByID(8, "DESCRIPTION"); ?>
<div class="grid grid--xl _ph-20">
    <div class="cell cell--24">
        <div class="grid _sv-15 _mb-20">
			<?php if ($iblockDescription): ?>
                <div class="cell cell--24">
                    <div class="w-faq-text"><?php echo $iblockDescription; ?></div>
                </div>
            <?php endif; ?>
            <div class="cell cell--14 cell--md-12 cell--ms-24">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:news.list", "faq", array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "COMPONENT_TEMPLATE" => "faq",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "8",
                    "IBLOCK_TYPE" => "references",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "2",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N"
                        ), false
                );
                ?>
            </div>
            <div class="cell cell--10 cell--md-12 cell--ms-24 _ph-50 _md-ph-20 _ms-ph-0 js-sticky-container">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:form.result.new", "question", Array(
                    "AJAX_MODE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_SHADOW" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "CHAIN_ITEM_LINK" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "EDIT_URL" => "",
                    "IGNORE_CUSTOM_TEMPLATE" => "Y",
                    "LIST_URL" => "",
                    "SEF_MODE" => "N",
                    "SUCCESS_URL" => "",
                    "USE_EXTENDED_ERRORS" => "Y",
                    "VARIABLE_ALIASES" => array("WEB_FORM_ID" => "WEB_FORM_ID", "RESULT_ID" => "RESULT_ID",),
                    "WEB_FORM_ID" => "2"
                        )
                );
                ?>
            </div>
        </div>
    </div>
</div>
<br>
<?php
/**
 * SEO
 */
$title = $APPLICATION->GetTitle('title');
$str = '';
if (isset($_GET["PAGEN_1"]) AND $_GET["PAGEN_1"] > 1) {

    $str .= $_GET["PAGEN_1"] . ' страница';
    $APPLICATION->SetPageProperty('title', $title . ' - ' . $str);
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>