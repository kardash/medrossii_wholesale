<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Натуральные продукты");
?>
<?php
$APPLICATION->IncludeComponent(
        "bitrix:news.list", "slider_main", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "N",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "3600",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "COMPONENT_TEMPLATE" => "slider_main",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => array(0 => "ID", 1 => "DETAIL_PICTURE", 2 => "",),
    "FILTER_NAME" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
    "IBLOCK_ID" => "25",
    "IBLOCK_TYPE" => "slider",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "INCLUDE_SUBSECTIONS" => "N",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "20",
    "PAGER_BASE_LINK" => "",
    "PAGER_BASE_LINK_ENABLE" => "Y",
    "PAGER_DESC_NUMBERING" => "Y",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_PARAMS_NAME" => "arrPager",
    "PAGER_SHOW_ALL" => "Y",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "PAGER_TITLE" => "Слайдер",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "",
    "PROPERTY_CODE" => array(0 => "", 1 => "DESCRIPTION", 2 => "",),
    "SET_BROWSER_TITLE" => "N",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "N",
    "SHOW_404" => "N",
    "SORT_BY1" => "SORT",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER1" => "ASC",
    "SORT_ORDER2" => "DESC",
    "STRICT_SECTION_CHECK" => "N"
        )
);
?>
<?php
$APPLICATION->IncludeComponent(
        "bitrix:news.list", "advantages", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "N",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "3600",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "COMPONENT_TEMPLATE" => "advantages",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => array(0 => "ID", 1 => "DETAIL_PICTURE", 2 => "",),
    "FILTER_NAME" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
    "IBLOCK_ID" => "26",
    "IBLOCK_TYPE" => "references",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "INCLUDE_SUBSECTIONS" => "N",
    "MEDIA_PROPERTY" => "",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "5",
    "PAGER_BASE_LINK" => "",
    "PAGER_BASE_LINK_ENABLE" => "Y",
    "PAGER_DESC_NUMBERING" => "Y",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_PARAMS_NAME" => "arrPager",
    "PAGER_SHOW_ALL" => "Y",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "PAGER_TITLE" => "Слайдер",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "",
    "PROPERTY_CODE" => array(0 => "", 1 => "DESCRIPTION", 2 => "",),
    "SEARCH_PAGE" => "/search/",
    "SET_BROWSER_TITLE" => "N",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "N",
    "SHOW_404" => "N",
    "SLIDER_PROPERTY" => "",
    "SORT_BY1" => "SORT",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER1" => "ASC",
    "SORT_ORDER2" => "DESC",
    "STRICT_SECTION_CHECK" => "N",
    "TEMPLATE_THEME" => "blue",
    "USE_RATING" => "N",
    "USE_SHARE" => "N"
        )
);
?>

<?php
$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "product_catalog", Array(
    "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
    "CACHE_GROUPS" => "Y", // Учитывать права доступа
    "CACHE_TIME" => "36000000", // Время кеширования (сек.)
    "CACHE_TYPE" => "A", // Тип кеширования
    "COUNT_ELEMENTS" => "Y", // Показывать количество элементов в разделе
    "IBLOCK_ID" => IBLOCK_CATALOG_ID, // Инфоблок
    "IBLOCK_TYPE" => "catalog", // Тип инфоблока
    "SECTION_CODE" => "", // Код раздела
    "SECTION_FIELDS" => array(// Поля разделов
        0 => "",
        1 => "",
    ),
    "SECTION_ID" => $_REQUEST["SECTION_ID"], // ID раздела
    "SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
    "SECTION_USER_FIELDS" => array(// Свойства разделов
        0 => "UF_ICON",
        1 => "",
    ),
    "SHOW_PARENT_NAME" => "Y", // Показывать название раздела
    "TOP_DEPTH" => "1", // Максимальная отображаемая глубина разделов
    "VIEW_MODE" => "LINE", // Вид списка подразделов
        ), false
);
?>
<?php
global $arrFilter;
$arrFilter = array();
$arrFilter['PROPERTY_SITE'] = 101;
?>
<?php
$APPLICATION->IncludeComponent(
        "bitrix:news.list", "banner_cooperation", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "N",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "3600",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "USE_FILTER" => "Y",
    "FILTER_NAME" => "arrFilter",
    "COMPONENT_TEMPLATE" => "banner_main",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => array(0 => "ID", 1 => "DETAIL_PICTURE", 2 => "",),
    "FILTER_NAME" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
    "IBLOCK_ID" => "28",
    "IBLOCK_TYPE" => "slider",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "INCLUDE_SUBSECTIONS" => "N",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "1",
    "PAGER_BASE_LINK" => "",
    "PAGER_BASE_LINK_ENABLE" => "Y",
    "PAGER_DESC_NUMBERING" => "Y",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_PARAMS_NAME" => "arrPager",
    "PAGER_SHOW_ALL" => "Y",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "PAGER_TITLE" => "Слайдер",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "",
    "PROPERTY_CODE" => array(0 => "", 1 => "DESCRIPTION", 2 => "",),
    "SET_BROWSER_TITLE" => "N",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "N",
    "SHOW_404" => "N",
    "SORT_BY1" => "SORT",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER1" => "ASC",
    "SORT_ORDER2" => "DESC",
    "STRICT_SECTION_CHECK" => "N"
        )
);
?>
<?php
$APPLICATION->IncludeComponent(
        "bitrix:news.list", "how_we_work", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "N",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "3600",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "COMPONENT_TEMPLATE" => "advantages",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => array(0 => "ID", 1 => "DETAIL_PICTURE", 2 => "",),
    "FILTER_NAME" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
    "IBLOCK_ID" => "27",
    "IBLOCK_TYPE" => "references",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "INCLUDE_SUBSECTIONS" => "N",
    "MEDIA_PROPERTY" => "",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "5",
    "PAGER_BASE_LINK" => "",
    "PAGER_BASE_LINK_ENABLE" => "Y",
    "PAGER_DESC_NUMBERING" => "Y",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_PARAMS_NAME" => "arrPager",
    "PAGER_SHOW_ALL" => "Y",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "PAGER_TITLE" => "Слайдер",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "",
    "PROPERTY_CODE" => array(0 => "", 1 => "DESCRIPTION", 2 => "",),
    "SEARCH_PAGE" => "/search/",
    "SET_BROWSER_TITLE" => "N",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "N",
    "SHOW_404" => "N",
    "SLIDER_PROPERTY" => "",
    "SORT_BY1" => "SORT",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER1" => "ASC",
    "SORT_ORDER2" => "DESC",
    "STRICT_SECTION_CHECK" => "N",
    "TEMPLATE_THEME" => "blue",
    "USE_RATING" => "N",
    "USE_SHARE" => "N",
    "COOPERATION_LINK" => LINK_TYPE
        )
);
?>
<?php
global $arrFilter;
$arrFilter = array();
$arrFilter['PROPERTY_SITE'] = 101;
?>
<?
$APPLICATION->IncludeComponent(
"bitrix:news.list", "banner_main", Array(
"ACTIVE_DATE_FORMAT" => "d.m.Y",
"ADD_SECTIONS_CHAIN" => "N",
"AJAX_MODE" => "Y",
"AJAX_OPTION_ADDITIONAL" => "",
"AJAX_OPTION_HISTORY" => "N",
"AJAX_OPTION_JUMP" => "N",
"AJAX_OPTION_STYLE" => "Y",
"CACHE_FILTER" => "Y",
"CACHE_GROUPS" => "Y",
"CACHE_TIME" => "3600",
"CACHE_TYPE" => "A",
"CHECK_DATES" => "Y",
"USE_FILTER" => "Y",
"FILTER_NAME" => "arrFilter",
"COMPONENT_TEMPLATE" => "banner_main",
"DETAIL_URL" => "",
"DISPLAY_BOTTOM_PAGER" => "N",
"DISPLAY_DATE" => "N",
"DISPLAY_NAME" => "Y",
"DISPLAY_PICTURE" => "Y",
"DISPLAY_PREVIEW_TEXT" => "Y",
"DISPLAY_TOP_PAGER" => "N",
"FIELD_CODE" => array(0 => "ID", 1 => "DETAIL_PICTURE", 2 => "",),
"FILTER_NAME" => "",
"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
"IBLOCK_ID" => "5",
"IBLOCK_TYPE" => "slider",
"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
"INCLUDE_SUBSECTIONS" => "N",
"MESSAGE_404" => "",
"NEWS_COUNT" => "1",
"PAGER_BASE_LINK" => "",
"PAGER_BASE_LINK_ENABLE" => "Y",
"PAGER_DESC_NUMBERING" => "Y",
"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
"PAGER_PARAMS_NAME" => "arrPager",
"PAGER_SHOW_ALL" => "Y",
"PAGER_SHOW_ALWAYS" => "N",
"PAGER_TEMPLATE" => "",
"PAGER_TITLE" => "Слайдер",
"PARENT_SECTION" => "",
"PARENT_SECTION_CODE" => "",
"PREVIEW_TRUNCATE_LEN" => "",
"PROPERTY_CODE" => array(0 => "", 1 => "DESCRIPTION", 2 => "",),
"SET_BROWSER_TITLE" => "N",
"SET_LAST_MODIFIED" => "N",
"SET_META_DESCRIPTION" => "N",
"SET_META_KEYWORDS" => "N",
"SET_STATUS_404" => "N",
"SET_TITLE" => "N",
"SHOW_404" => "N",
"SORT_BY1" => "SORT",
"SORT_BY2" => "ACTIVE_FROM",
"SORT_ORDER1" => "ASC",
"SORT_ORDER2" => "DESC",
"STRICT_SECTION_CHECK" => "N"
)
);
?>

<div class="w-index-seo-text w-index-seo-text--opt">
    <div class="grid grid--xl _ph-20">
        <div class="cell cell--24">
            <div class="grid _justify-center _s-30 _ms-s-15">
                <div class="cell cell--24 _text-center">
                    <h1 class="w-title w-title--center w-title--big"><?php   echo $APPLICATION->ShowTitle(); ?></h1>
                            <div class="w-title-desc w-title-desc--big">
                                с уникальными свойствами
                            </div>
                            <div class="w-title-line">
                            </div>
                        </div>
                        <div class="cell cell--12 cell--ms-24">
                            <div class="w-index-seo-text__image">
                                <?
                                $APPLICATION->IncludeComponent(
                                "bitrix:main.include", "", Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "/include/wholesale/index_image.php"
                                )
                                );
                                ?>
                            </div>
                        </div>
                        <div class="cell cell--12 cell--ms-24">
                            <div class="w-index-seo-text__desc w-text">
                                <?
                                $APPLICATION->IncludeComponent(
                                "bitrix:main.include", "", Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "/include/wholesale/index_text.php"
                                )
                                );
                                ?>
                            </div>
                            <div class="_mt-40 _ms-text-center">
                                <a href="<?php echo LINK_TYPE . '/about/'; ?>" class="w-button w-button--trans w-button--lower">
                                    <span>Подробнее о компании</span>
                                    <svg><use xlink:href="<?php echo LINK_TYPE . SITE_TEMPLATE_PATH; ?>/svg/sprite.svg#icon-arrow-right"></use></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require($_SERVER[ "DOCUMENT_ROOT"  ] . "/bitrix/footer.php"); ?>