<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Каталог товаров");
?>

<?php
$sortField = "shows";
$sortOrder = "desc";
$sortField2 = "CATALOG_AVAILABLE";
$sortOrder2 = "desc";

if ($_GET["sort"] == "shows" OR $_GET["sort"] == "catalog_PRICE_8" OR $_GET["sort"] == "name" OR $_GET["sort"] == "PROPERTY_NEWPRODUCT") {
    $sortField = $_GET["sort"];
    $sortOrder = $_GET["order"];
}
?>
<?php
$APPLICATION->IncludeComponent(
        "bitrix:catalog", "catalog_wholesale", array(
    "IBLOCK_TYPE" => "catalog",
    "IBLOCK_ID" => IBLOCK_CATALOG_ID,
    "TEMPLATE_THEME" => "",
    "HIDE_NOT_AVAILABLE" => "N",
    "BASKET_URL" => "/personal/cart/",
    "ACTION_VARIABLE" => "action",
    "PRODUCT_ID_VARIABLE" => "id",
    "SECTION_ID_VARIABLE" => "SECTION_ID",
    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
    "PRODUCT_PROPS_VARIABLE" => "prop",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "/catalog/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "Y",
    "SET_TITLE" => "Y",
    "ADD_SECTION_CHAIN" => "Y",
    "ADD_ELEMENT_CHAIN" => "Y",
    "SET_STATUS_404" => "Y",
    "DETAIL_DISPLAY_NAME" => "N",
    "USE_ELEMENT_COUNTER" => "Y",
    "USE_FILTER" => "Y",
    "FILTER_NAME" => "",
    "FILTER_VIEW_MODE" => "VERTICAL",
    "FILTER_FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "FILTER_PROPERTY_CODE" => array(
        0 => "COLLECTION_REGION",
        1 => "HONEY_TYPE",
        2 => "",
    ),
    "FILTER_PRICE_CODE" => array(
    ),
    "FILTER_OFFERS_FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "FILTER_OFFERS_PROPERTY_CODE" => array(
        0 => "",
        1 => "",
    ),
    "USE_REVIEW" => "Y",
    "MESSAGES_PER_PAGE" => "10",
    "USE_CAPTCHA" => "Y",
    "REVIEW_AJAX_POST" => "Y",
    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
    "FORUM_ID" => "1",
    "URL_TEMPLATES_READ" => "",
    "SHOW_LINK_TO_FORUM" => "Y",
    "USE_COMPARE" => "N",
    "PRICE_CODE" => array(
        0 => "WHOLESALE",
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "PRICE_VAT_SHOW_VALUE" => "N",
    "PRODUCT_PROPERTIES" => array(
    ),
    "USE_PRODUCT_QUANTITY" => "Y",
    "CONVERT_CURRENCY" => "Y",
    "QUANTITY_FLOAT" => "N",
    "OFFERS_CART_PROPERTIES" => array(
        0 => "VES",
    ),
    "SHOW_TOP_ELEMENTS" => "N",
    "SECTION_COUNT_ELEMENTS" => "N",
    "SECTION_TOP_DEPTH" => "1",
    "SECTIONS_VIEW_MODE" => "TILE",
    "SECTIONS_SHOW_PARENT_NAME" => "N",
    "PAGE_ELEMENT_COUNT" => "15",
    "LINE_ELEMENT_COUNT" => "3",
    "ELEMENT_SORT_FIELD" => $sortField,
    "ELEMENT_SORT_ORDER" => $sortOrder,
    "ELEMENT_SORT_FIELD2" => $sortField2,
    "ELEMENT_SORT_ORDER2" => $sortOrder2,
    "LIST_PROPERTY_CODE" => array(
        0 => "CML2_ARTICLE",
        1 => "YEAR_COLLECTION",
        2 => "NEWPRODUCT",
        3 => "SHOW_TEXT_BLOCK",
        4 => "COLLECTION_REGION",
        5 => "HONEY_TYPE",
        6 => "SALELEADER",
        7 => "FORUM_MESSAGE_CNT",
        8 => "",
    ),
    "INCLUDE_SUBSECTIONS" => "Y",
    "LIST_META_KEYWORDS" => "-",
    "LIST_META_DESCRIPTION" => "-",
    "LIST_BROWSER_TITLE" => "-",
    "LIST_OFFERS_FIELD_CODE" => array(
        0 => "NAME",
        1 => "PREVIEW_PICTURE",
        2 => "DETAIL_PICTURE",
        3 => "",
    ),
    "LIST_OFFERS_PROPERTY_CODE" => array(
        0 => "VES",
        1 => "",
    ),
    "LIST_OFFERS_LIMIT" => "0",
    "SECTION_BACKGROUND_IMAGE" => "-",
    "DETAIL_PROPERTY_CODE" => array(
        0 => "PROMOTION",
        1 => "CML2_ARTICLE",
        2 => "YEAR_COLLECTION",
        3 => "NEWPRODUCT",
        4 => "SHOW_TEXT_BLOCK",
        5 => "COLLECTION_REGION",
        6 => "HONEY_TYPE",
        7 => "SALELEADER",
        8 => "FORUM_MESSAGE_CNT",
        9 => "",
    ),
    "DETAIL_META_KEYWORDS" => "-",
    "DETAIL_META_DESCRIPTION" => "-",
    "DETAIL_BROWSER_TITLE" => "-",
    "DETAIL_OFFERS_FIELD_CODE" => array(
        0 => "NAME",
        1 => "",
    ),
    "DETAIL_OFFERS_PROPERTY_CODE" => array(
        0 => "VES",
        1 => "",
    ),
    "DETAIL_BACKGROUND_IMAGE" => "-",
    "LINK_IBLOCK_TYPE" => "offers",
    "LINK_IBLOCK_ID" => "21",
    "LINK_PROPERTY_SID" => "CML2_LINK",
    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
    "USE_ALSO_BUY" => "Y",
    "ALSO_BUY_ELEMENT_COUNT" => "4",
    "ALSO_BUY_MIN_BUYES" => "1",
    "OFFERS_SORT_FIELD" => "sort",
    "OFFERS_SORT_ORDER" => "desc",
    "OFFERS_SORT_FIELD2" => "id",
    "OFFERS_SORT_ORDER2" => "desc",
    "PAGER_TEMPLATE" => ".default",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Товары",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
    "PAGER_SHOW_ALL" => "N",
    "ADD_PICT_PROP" => "-",
    "LABEL_PROP" => array(
        0 => "NEWPRODUCT",
        1 => "SALELEADER",
    ),
    "PRODUCT_DISPLAY_MODE" => "Y",
    "OFFER_ADD_PICT_PROP" => "-",
    "OFFER_TREE_PROPS" => array(
    ),
    "SHOW_DISCOUNT_PERCENT" => "N",
    "SHOW_OLD_PRICE" => "Y",
    "MESS_BTN_BUY" => "Купить",
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
    "MESS_BTN_COMPARE" => "Сравнение",
    "MESS_BTN_DETAIL" => "Подробнее",
    "MESS_NOT_AVAILABLE" => "Нет в наличии",
    "DETAIL_USE_VOTE_RATING" => "Y",
    "DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
    "DETAIL_USE_COMMENTS" => "Y",
    "DETAIL_BLOG_USE" => "Y",
    "DETAIL_VK_USE" => "N",
    "DETAIL_FB_USE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "USE_STORE" => "Y",
    "BIG_DATA_RCM_TYPE" => "personal",
    "FIELDS" => array(
        0 => "",
        1 => "STORE",
        2 => "",
    ),
    "USE_MIN_AMOUNT" => "Y",
    "STORE_PATH" => "/store/#store_id#",
    "MAIN_TITLE" => "Наличие на складах",
    "MIN_AMOUNT" => "11",
    "DETAIL_BRAND_USE" => "N",
    "DETAIL_BRAND_PROP_CODE" => array(
        0 => "",
        1 => "BRAND_REF",
        2 => "",
    ),
    "SIDEBAR_SECTION_SHOW" => "Y",
    "SIDEBAR_DETAIL_SHOW" => "Y",
    "SIDEBAR_PATH" => "/catalog/sidebar.php",
    "COMPONENT_TEMPLATE" => "catalog_wholesale",
    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
    "LABEL_PROP_MOBILE" => array(
    ),
    "LABEL_PROP_POSITION" => "top-right",
    "COMMON_SHOW_CLOSE_POPUP" => "Y",
    "PRODUCT_SUBSCRIPTION" => "Y",
    "DISCOUNT_PERCENT_POSITION" => "top-right",
    "SHOW_MAX_QUANTITY" => "M",
    "MESS_BTN_SUBSCRIBE" => "Сообщить о наличии",
    "USE_MAIN_ELEMENT_SECTION" => "Y",
    "DETAIL_STRICT_SECTION_CHECK" => "N",
    "SET_LAST_MODIFIED" => "N",
    "ADD_SECTIONS_CHAIN" => "Y",
    "USE_SALE_BESTSELLERS" => "Y",
    "FILTER_HIDE_ON_MOBILE" => "N",
    "INSTANT_RELOAD" => "N",
    "ADD_PROPERTIES_TO_BASKET" => "Y",
    "PARTIAL_PRODUCT_PROPERTIES" => "N",
    "USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
    "COMMON_ADD_TO_BASKET_ACTION" => "ADD",
    "TOP_ADD_TO_BASKET_ACTION" => "ADD",
    "SECTION_ADD_TO_BASKET_ACTION" => "ADD",
    "DETAIL_ADD_TO_BASKET_ACTION" => array(
        0 => "ADD",
    ),
    "DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => array(
        0 => "ADD",
    ),
    "SEARCH_PAGE_RESULT_COUNT" => "16",
    "SEARCH_RESTART" => "N",
    "SEARCH_NO_WORD_LOGIC" => "Y",
    "SEARCH_USE_LANGUAGE_GUESS" => "Y",
    "SEARCH_CHECK_DATES" => "Y",
    "SECTIONS_HIDE_SECTION_NAME" => "N",
    "LIST_PROPERTY_CODE_MOBILE" => array(
    ),
    "LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
    "LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
    "LIST_ENLARGE_PRODUCT" => "STRICT",
    "LIST_SHOW_SLIDER" => "N",
    "LIST_SLIDER_INTERVAL" => "3000",
    "LIST_SLIDER_PROGRESS" => "N",
    "DETAIL_SET_CANONICAL_URL" => "N",
    "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
    "SHOW_DEACTIVATED" => "N",
    "DETAIL_MAIN_BLOCK_PROPERTY_CODE" => array(
        0 => "COLLECTION_REGION",
    ),
    "DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(
    ),
    "DETAIL_BLOG_URL" => "catalog_comments",
    "DETAIL_BLOG_EMAIL_NOTIFY" => "N",
    "DETAIL_FB_APP_ID" => "",
    "DETAIL_IMAGE_RESOLUTION" => "16by9",
    "DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
    "DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
    "DETAIL_SHOW_SLIDER" => "N",
    "DETAIL_DETAIL_PICTURE_MODE" => array(
    ),
    "DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
    "DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
    "MESS_PRICE_RANGES_TITLE" => "Цены",
    "MESS_DESCRIPTION_TAB" => "Описание",
    "MESS_PROPERTIES_TAB" => "Характеристики",
    "MESS_COMMENTS_TAB" => "Комментарии",
    "DETAIL_SHOW_POPULAR" => "N",
    "DETAIL_SHOW_VIEWED" => "Y",
    "USE_GIFTS_DETAIL" => "N",
    "USE_GIFTS_SECTION" => "N",
    "USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
    "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
    "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
    "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
    "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
    "GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
    "GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
    "GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
    "GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
    "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
    "GIFTS_SHOW_OLD_PRICE" => "Y",
    "GIFTS_SHOW_NAME" => "Y",
    "GIFTS_SHOW_IMAGE" => "Y",
    "GIFTS_MESS_BTN_BUY" => "Выбрать",
    "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
    "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
    "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
    "STORES" => array(
        0 => "",
        1 => "",
    ),
    "USER_FIELDS" => array(
        0 => "",
        1 => "",
    ),
    "SHOW_EMPTY_STORE" => "N",
    "SHOW_GENERAL_STORE_INFORMATION" => "N",
    "USE_BIG_DATA" => "Y",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "LAZY_LOAD" => "N",
    "LOAD_ON_SCROLL" => "N",
    "SHOW_404" => "Y",
    "MESSAGE_404" => "",
    "COMPATIBLE_MODE" => "Y",
    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
    "DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
    "USE_ENHANCED_ECOMMERCE" => "N",
    "FILE_404" => "",
    "CURRENCY_ID" => "RUB",
    "USER_CONSENT" => "N",
    "USER_CONSENT_ID" => "0",
    "USER_CONSENT_IS_CHECKED" => "Y",
    "USER_CONSENT_IS_LOADED" => "N",
    "MESS_SHOW_MAX_QUANTITY" => "В наличии",
    "RELATIVE_QUANTITY_FACTOR" => "5",
    "MESS_RELATIVE_QUANTITY_MANY" => "Много",
    "MESS_RELATIVE_QUANTITY_FEW" => "Мало",
    "SEF_URL_TEMPLATES" => array(
        "sections" => "",
        "section" => "#SECTION_CODE#/",
        "element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
        "compare" => "compare/",
        "smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
    )
        ), false
);
?>

<?php
/**
 * SEO
 */
$title = $APPLICATION->GetTitle('title');
$str = '';
if ((isset($_GET['sort']) AND isset($_GET['order'])) OR isset($_GET["PAGEN_1"])) {
    $sort = $_GET['sort'];
    $order = $_GET['order'];
    if ($sort == 'name') {
        $str .= 'по алфавиту ';
    } else if ($sort == 'shows') {
        $str .= 'по популярности ';
    } else if ($sort == 'catalog_PRICE_7') {
        $str .= 'по цене ';
    } else if ($sort == 'PROPERTY_NEWPRODUCT') {
        $str .= 'по новизне ';
    }
    if ($order == 'asc') {
        $str .= 'по возростанию';
    } else if ($order == 'desc') {
        $str .= 'по убыванию';
    }
    if (isset($_GET["PAGEN_1"])) {
        $str .= $_GET["PAGEN_1"] . ' страница';
    }
    $APPLICATION->SetPageProperty('title', $title . ' - ' . $str);
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>