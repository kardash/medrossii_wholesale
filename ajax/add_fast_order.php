<?php

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
/* die(json_encode(array(
  'error' => true,
  'message' => 'fdgfhfgj'
  ))); */
global $USER;
$name = $_POST['name'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$items = json_decode($_POST['goods']);

global $USER, $APPLICATION;

use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Main\Application,
    Bitrix\Sale\DiscountCouponsManager;

if (!Loader::IncludeModule('sale'))
    die();
$request = Application::getInstance()->getContext()->getRequest();
$currencyCode = 'RUB';

if ($USER->IsAuthorized()) {
    $registeredUserID = $USER->GetID();
} else {
    $rsUsers = CUser::GetList(($by = "id"), ($order = "desc"), array("EMAIL" => $email));
    if ($arUser = $rsUsers->Fetch()) {
        $registeredUserID = $arUser["ID"];
        if ($registeredUserID) {
            $arGroups = CUser::GetUserGroup($registeredUserID);
            if (!empty($arGroups) AND ! in_array(9, $arGroups)) {
                die(json_encode(array(
                    'error' => true,
                    'message' => 'Пользователь с указанным E-mail уже зарегистрирован как розничный покупатель. Если это Вы, пожалуйста, воспользуйтесь розничным магазином или зарегистрируйтесь как оптовый покупатель, используя другой E-mail',
                )));
            }
        }
    } else {
        $obUser = new CUser;

        $userLogin = $email;
        $userPassw = GetRandomCode();

        $arFields = Array(
            "EMAIL" => $email,
            "LOGIN" => $userLogin,
            "NAME" => $name,
            "PERSONAL_PHONE" => $phone,
            "LID" => SITE_ID,
            "ACTIVE" => "Y",
            "GROUP_ID" => array(3, 4, 6, 9),
            "PASSWORD" => $userPassw,
            "CONFIRM_PASSWORD" => $userPassw
        );
        $registeredUserID = $obUser->Add($arFields);
    }
}

$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
// $registeredUserID = $USER->GetID(); // или тут присваиваем идентификатор анонимного пользователя

$order = Order::create($siteId, $registeredUserID);
$order->setPersonTypeId(2); // ИД типа пользователя


$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), Bitrix\Main\Context::getCurrent()->getSite())->getOrderableItems();

$order->setBasket($basket);

$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem();

$shipment->setFields(array(
    'DELIVERY_ID' => 33,
    'DELIVERY_NAME' => 'Доставка товаров в быстром заказе',
    'CURRENCY' => $order->getCurrency()
));
$shipmentItemCollection = $shipment->getShipmentItemCollection();

foreach ($order->getBasket() as $item) {
    $shipmentItem = $shipmentItemCollection->createItem($item);
    $shipmentItem->setQuantity($item->getQuantity());
}

$paymentCollection = $order->getPaymentCollection();
$extPayment = $paymentCollection->createItem();
$extPayment->setFields(array(
    'PAY_SYSTEM_ID' => 1,
    'PAY_SYSTEM_NAME' => 'Наличные',
    'SUM' => $order->getPrice()
));

$order->doFinalAction(true);
$propertyCollection = $order->getPropertyCollection();

foreach ($propertyCollection->getGroups() as $group) {
    foreach ($propertyCollection->getGroupProperties($group['ID']) as $property) {
        $p = $property->getProperty();
        /* if ($p["CODE"] == "CONTACT_PERSON")
          $property->setValue("VASYA"); */
    }
}

$order->setField('CURRENCY', $currencyCode);
$order->setField('COMMENTS', 'Быстрый заказ с корзины. ' . $comment);
$r = $order->save();
$orderId = $order->GetId();
if (!$r->isSuccess()) {
    /* if ($ex = $APPLICATION->GetException())
      echo $aaaa = $ex->GetString(); */

    // либо объекты ошибок с доп данными
    /* print_r($r->getErrors());
      // либо только сообщения
      print_r($r->getErrorMessages()); */
    echo json_encode(array(
        'error' => true,
        'message' => 'Не удалось оформить заказ. Попробуйте, пожалуйста, позже. ' . $r->getErrorMessages(),
    ));

    // так же в заказе могут быть предупреждения, которые не являются причиной остановки процесса сохранения заказа, но мы их сохраняем в маркировки
    /* print_r($r->getWarnings());
      print_r($r->getWarningMessages()); */
} else {
    echo json_encode(array(
        'success' => true,
        'message' => 'Ваш заказ №' . $orderId . ' оформлен. <br/>Менеджер  свяжется с Вами в ближайшее время.',
        'timeout' => 5000,
        'redirect' => LINK_TYPE
    ));
}
?>