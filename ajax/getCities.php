<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?php 
	Bitrix\Main\Loader::includeModule('sale');
	$db_vars = CSaleLocation::GetList(
		 array(
				 "SORT" => "ASC",
				 "COUNTRY_NAME_LANG" => "ASC",
				 "CITY_NAME_LANG" => "ASC"
			 ),
		 array("LID" => LANGUAGE_ID, "%CITY_NAME_LANG" => $_POST['query']),
		 false,
		 false,
		 array()
	 );
	$list = '';
	while ($vars = $db_vars->Fetch()):
	  
	   $list .= '<li><div class="js-city-choose">'.htmlspecialchars($vars["CITY_NAME"]).'</div></li>';
	   
	endwhile;

	echo json_encode(array(
		'show' => true,
		'list' => $list
	));
?>
